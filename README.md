package daspro.recruitment;

import java.util.Scanner;

public class No1 {

    public static void main(String[] args) {
        Scanner br = new Scanner(System.in);

        System.out.println("## KETENTUAN ##");
        System.out.println("1. Angka N Tidak Boleh melebihi 1.000.000.000");
        System.out.println("2. Angka M Tidak Boleh melebihi 1.000.000.000");
        System.out.println();
        
        System.out.print("Masukkan Angka N M: ");
        double N = br.nextDouble();
        double M = br.nextDouble();

        int mds = (int) (N % M);

        int a = (int) (N - M);
        int total = 0;
        if (N >= 1 && N <= 1000000000 && M >= 1 && M <= 1000000000) {
        int i = 0;
            if (mds > 0) {

                while (N >= M) {
                    N -= M;
                    i++;
                }
                System.out.println("Kandang yang dibutuhkan berjumlah: "+(i+1));
            } else {
                System.out.println("Kandang yang dibutuhkan berjumlah: "+N / M);
            }
        } else {
            System.out.println("INVALID");
        }

    }
}
